package com.dorrpon.mynote;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {


    private Context context;
    private Note[] notes;

    public NoteAdapter(Context context, Note[] notes) {
        this.notes = notes;
        this.context = context;
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.single_note, viewGroup, false);
        return new NoteViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder NoteViewHolder, int i) {
        final Note note = notes[i];

        NoteViewHolder.noteName.setText(note.getTitle());

        // Glide.with(githubViewHolder.image.getContext()).load(user.getAvatarUrl()).into(githubViewHolder.image);


        NoteViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "" + note.getNote(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, SingleActivity.class);
                intent.putExtra("id", note.getId().toString());

                //not recommanded
               intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return notes.length;
    }

    public class NoteViewHolder extends RecyclerView.ViewHolder {
        TextView noteName;

        public NoteViewHolder(@NonNull View itemView) {
            super(itemView);
            noteName = itemView.findViewById(R.id.title);
        }
    }
}
